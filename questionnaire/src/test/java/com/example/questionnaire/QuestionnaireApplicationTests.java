package com.example.questionnaire;

import com.example.questionnaire.model.Question;
import com.example.questionnaire.repository.QuestionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class QuestionnaireApplicationTests {

	@Autowired
	private QuestionRepository questionRepository;

	@Test
	void save_PersistANewPassengerWithProperties() {
		Question P = new Question();
		Question P2 = questionRepository.save(P);
		Assertions.assertNotNull(P2);
	}

}
