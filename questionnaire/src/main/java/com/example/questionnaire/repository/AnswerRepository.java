package com.example.questionnaire.repository;

import com.example.questionnaire.model.Answer;
import com.example.questionnaire.model.Question;
import com.example.questionnaire.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Integer>{

    @Query("Select f from User f where f.email = :email")
    public List<Answer> getAnswerByEmail(@Param("email") String email);

    @Query("Select f from Answer f")
    public ArrayList<Answer> getAllAnswers();

}
