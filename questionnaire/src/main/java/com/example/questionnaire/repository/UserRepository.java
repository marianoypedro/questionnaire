package com.example.questionnaire.repository;

import com.example.questionnaire.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface UserRepository extends JpaRepository<User, String>{
    @Query("Select f from User f where f.email = :email")
    public User getUserByEmail(@Param("email") String email);

    @Query("Select f from User f")
    public ArrayList<User> getAllUsers();
}
