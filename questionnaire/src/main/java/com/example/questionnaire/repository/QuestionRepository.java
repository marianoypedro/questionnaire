package com.example.questionnaire.repository;

import com.example.questionnaire.model.Question;
import com.example.questionnaire.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Integer>{

    @Query("Select f from Question f")
    public ArrayList<Question> getAllQuestions();


}
