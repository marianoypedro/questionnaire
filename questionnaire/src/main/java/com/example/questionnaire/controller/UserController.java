package com.example.questionnaire.controller;

import com.example.questionnaire.model.User;
import com.example.questionnaire.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/User/")
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@PostMapping("addUser")
	public User createUser(@RequestBody User user){
		return userRepository.save(user);
	}

	@GetMapping("getUser")
	public ArrayList<User> getUser(){
		return userRepository.getAllUsers();
	}

}

