package com.example.questionnaire.controller;

import com.example.questionnaire.model.Question;
import com.example.questionnaire.model.User;
import com.example.questionnaire.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/Question/")
public class QuestionController {

	@Autowired
	private QuestionRepository questionRepository;

	@PostMapping("addQuestion")
	public Question addQuestion(@RequestBody Question Question){
		return questionRepository.save(Question);
	}

	@GetMapping("getQuestion")
	public ArrayList<Question> getQuestion(){
		return questionRepository.getAllQuestions();
	}

}
