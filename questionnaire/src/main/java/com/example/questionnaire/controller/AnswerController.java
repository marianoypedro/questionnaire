package com.example.questionnaire.controller;

import com.example.questionnaire.model.Answer;
import com.example.questionnaire.model.Question;
import com.example.questionnaire.repository.AnswerRepository;
import com.example.questionnaire.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/Answer/")
public class AnswerController {

	@Autowired
	private AnswerRepository answerRepository;

	@PostMapping("addAnswer")
	public Answer createAnswer(@RequestBody Answer answer){
		return answerRepository.save(answer);
	}

	@PostMapping("getAnswerByEmail")
	public List<Answer> getAnswerByEmail(@RequestBody String email){
		return answerRepository.getAnswerByEmail(email);
	}

	@GetMapping("getAnswer")
	public ArrayList<Answer> getAllAnswers(){
		return answerRepository.getAllAnswers();
	}

}
