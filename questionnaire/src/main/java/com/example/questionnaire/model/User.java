package com.example.questionnaire.model;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {

	@Id
	@Column(name = "email")
	private String email;


	public User(String email, Integer results) {
		this.email = email;
	}
	public User() {
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
