package com.example.questionnaire.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "question")
public class Question {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "question")
	private String question;

	@Column(name = "multiple1")
	private String multiple1;
	@Column(name = "multiple2")
	private String multiple2;
	@Column(name = "multiple3")
	private String multiple3;
	@Column(name = "multiple4")
	private String multiple4;
	@Column(name = "multiple5")
	private String multiple5;

	@Column(name = "type")
	private Integer type;

	public Question(Integer id, String question, String multiple1, String multiple2, String multiple3, String multiple4, String multiple5, Integer type) {
		this.question = question;
		this.multiple1 = multiple1;
		this.multiple2 = multiple2;
		this.multiple3 = multiple3;
		this.multiple4 = multiple4;
		this.multiple5 = multiple5;
		this.type = type;
	}

	public String getMultiple1() {
		return multiple1;
	}

	public void setMultiple1(String multiple1) {
		this.multiple1 = multiple1;
	}

	public String getMultiple2() {
		return multiple2;
	}

	public void setMultiple2(String multiple2) {
		this.multiple2 = multiple2;
	}

	public String getMultiple3() {
		return multiple3;
	}

	public void setMultiple3(String multiple3) {
		this.multiple3 = multiple3;
	}

	public String getMultiple4() {
		return multiple4;
	}

	public void setMultiple4(String multiple4) {
		this.multiple4 = multiple4;
	}

	public String getMultiple5() {
		return multiple5;
	}

	public void setMultiple5(String multiple5) {
		this.multiple5 = multiple5;
	}

	public Question(){
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
}
