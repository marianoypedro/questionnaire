package com.example.questionnaire.model;

import javax.persistence.*;

@Entity
@Table(name = "question")
public class Answer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "answer")
	private String answer;

	@Column(name = "questionid")
	private Integer questionid;

	@Column(name = "email")
	private String  email;

	public Answer(String answer, Integer questionid, String email) {
		this.answer = answer;
		this.questionid = questionid;
		this.email = email;
	}

	public Answer() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Integer getQuestionid() {
		return questionid;
	}

	public void setQuestionid(Integer questionid) {
		this.questionid = questionid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
